#include <stdlib.h>
#include "ImageViewer.h"
#include "Image.h"
#include <iostream> 
#include <cassert> 

using namespace std;


ImageViewer::ImageViewer() :m_window(nullptr),m_surface(nullptr), m_texture(nullptr) {
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    m_window = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (m_window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }
    m_renderer = SDL_CreateRenderer(m_window, -1,SDL_RENDERER_ACCELERATED); 
}

ImageViewer::~ImageViewer(){
    if (m_texture != nullptr) {
        SDL_DestroyTexture(m_texture);
    }
	if (m_surface != nullptr) {
        SDL_FreeSurface(m_surface);
    }
}


// Affiche l’image passée en paramètre et permet le (dé)zoom
void ImageViewer::afficher(const Image& im){

    m_surface = SDL_CreateRGBSurfaceFrom(im.getTab(),61,61,24,61*3,255,255,255,0);

    m_texture = SDL_CreateTextureFromSurface(m_renderer,m_surface);
    
    float zoom = 1.0;

    SDL_Rect r;
    r.x = static_cast<int>(200-200*zoom); 
    r.y = static_cast<int>(200-200*zoom);
    r.w = 200*zoom;
    r.h = 200*zoom;


    SDL_RenderClear(m_renderer);

    SDL_RenderCopy(m_renderer, m_texture, NULL, &r);

    SDL_RenderPresent(m_renderer);

	bool quit = false;
    SDL_Event event;

	// tant que ce n'est pas la fin ...
	while (!quit) {

		// tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
			else if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.scancode==SDL_SCANCODE_ESCAPE) 
                {
                        quit = true;
				}
                else if (event.key.keysym.scancode==SDL_SCANCODE_T) 
                {
                        zoom *= 1.05;
				}
                else if (event.key.keysym.scancode==SDL_SCANCODE_G)
                {
                        zoom *= 0.95;
				}
            
                }
            }
    }
}

