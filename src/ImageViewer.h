#ifndef __IMAGEVIEWER__H
#define __IMAGEVIEWER__H


#include "Image.h"
#include <SDL2/SDL.h>


/**
 * @class ImageViewer
 * 
 * @brief Classe ImageViewer permettant l'affichage d'une image
 * 
*/
class ImageViewer {
    private:    

    //lien sur SDL_Window
        SDL_Window * m_window;
    //lien sur SDL_Renderer
        SDL_Renderer * m_renderer;
    // éventuellement d’autres données (ex. SDL_Surface et SDL_Texture)
        SDL_Surface * m_surface;
        SDL_Texture * m_texture;
        bool m_hasChanged;
        


    public:
        /**
        * @brief Constructeur qui initialise tout SDL2 et crée la fenêtre et le renderer
        * 
        * */ 
        ImageViewer();

        /**
         * @brief Détruit et ferme SDL2
         * 
         * */
        ~ImageViewer();

        /**
         * @brief Affiche l’image passée en paramètre et permet le (dé)zoom
         * @param im une image
         * 
         * */
        void afficher (const Image& im);
};

#endif