#include <iostream>
#include <cassert>
#include <fstream>
#include "Image.h"

using namespace std;

/* Constructeur de la classe : initialise dimx et dimy à 0 n’alloue aucune mémoire pour le tableau de pixel*/
Image::Image(){
    dimx = 0;
    dimy = 0;
    tab = NULL;
}
/*Constructeur de la classe : initialise dimx et dimy (après vérification) puis alloue le tableau de pixel dans le tas (image noire)*/
Image::Image(const int& dimx,const int& dimy) {
    //Vérification de dimx et dimy
    this->dimx = dimx;
    this-> dimy = dimy;
    this->tab = new Pixel[dimx * dimy]; //allocation
}

/*Destructeur de la classe : déallocation de la mémoire du tableau de pixel et mise à jour des champs dimx et dimy à 0*/
Image::~Image(){
    //Mise à 0 de dimx et dimy
    if(dimx != 0 && dimy !=0){
        dimx = 0;
        dimy = 0;
    }
    //On libère le tableau de pixels
    if(tab!=NULL){
        delete [] tab;
        tab = NULL;
    }
}

//Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
Pixel& Image::getPix(const int& x,const int& y) {
    //Tests : on vérifie que les dimensions sont bonnes et que le tableau a été alloué
    assert(tab != NULL);
    assert(x<dimx && y < dimy);
    //Récupération des coordonnées
    return tab[dimx * y + x];
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité. Renvoie une copie du pixel.
Pixel Image::getPix(const int& x, const int& y) const{
    //Tests : on vérifie que les dimensions sont bonnes et que le tableau a été alloué
    assert(tab != NULL);
    assert(x<dimx && y < dimy);
    //Récupération des coordonnées
    return tab[dimx * y + x];
}


// Modifie le pixel de coordonnées (x,y)
void Image::setPix(const int& x,const int& y, const Pixel& couleur){
    //Tests : on vérifie que les dimensions sont bonnes
    assert(x >= 0 && x<dimx);
    assert(y >= 0 && y<dimy);
    //Modification de la couleur du pixel
    tab[dimx * y + x]=couleur;
}

// Dessine un rectangle plein de la couleur dans l'image (en utilisant setPix, indices en paramètre compris)
void Image::dessinerRectangle(const int& Xmin, const int& Ymin, const int& Xmax, const int& Ymax, const Pixel& couleur){
    //Coordonnées min dans l'image
    assert(Xmin <= dimx);
    assert(Ymin <= dimy);
    //Coordonnées max dans l'image
    assert(Xmax <= dimx);
    assert(Ymax <= dimy);
    // On vérifie que les coordonnées min ne soient pas plus grands que les coordonnées max 
    assert(Xmin <= Xmax);
    assert(Ymin <= Ymax);

    int i,j;
    //On parcourt l'image
    for(i=Xmin;i<Xmax;i++)
    {
        for(j=Ymin;j<Ymax;j++)
        {
            setPix(i,j,couleur); //On change le pixel par la couleur
        }
    }
}

// Efface l'image en la remplissant de la couleur en paramètre
void Image::effacer(const Pixel& couleur){
    dessinerRectangle(0,0,dimx,dimy, couleur);
}

// Sauve une image dans un fichier
void Image::sauver(const string &filename) const
{
    int x,y;
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (y = 0; y < dimy; ++y)
        for (x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

// Ouvre une image depuis un fichier
void Image::ouvrir(const string &filename)
{
    int x,y;
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    unsigned char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (y = 0; y < dimy; ++y)
        for (x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

// Affiche les valeurs des pixels sur la console
void Image::afficherConsole()
{
    int x,y;
    cout << dimx << " " << dimy << endl;
    for (y = 0; y < dimy; ++y)
    {
        for (x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

int Image::getDimx() const{
    return dimx;
}

// Récupère la valeur de dimy
int Image::getDimy() const{
    return dimy;
}

void * Image::getTab() const{
    return tab;
}

/*Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et
font bien ce qu’elles sont censées faire, ainsi que les données membres de
l'objet sont conformes*/
void Image::testRegression (){
    //Initialisation des pixels pour le test des fonctions
    Pixel p(100,0,0);
    Pixel p2(200,200,200);
    //Test du 1er constructeur (sans l'allocation mémoire)
    Image i;
    assert(i.dimx==0);
    assert(i.dimy == 0);
    assert(i.tab == NULL);
    //Test du 2e constructeur avec l'allocation mémoire + les fonctions
    Image i2(1000,1000);
    assert(i2.getDimx() == 1000);
    assert(i2.getDimy() == 1000);
    i2.getPix(500,500);
    i2.setPix(700,700,p); //un pixel rouge
    assert(i2.tab->r == p.r || i2.tab->g == p.g || i2.tab->b == p.b);
    i2.dessinerRectangle(100,100,500,500,p); // Rectangle de couleur rouge
    i2.effacer(p2);
    assert(i2.tab->r == p2.r || i2.tab->g == p2.g || i2.tab->b == p2.b);
}