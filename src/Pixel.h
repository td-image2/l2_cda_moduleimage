#ifndef _PIXEL_H
#define _PIXEL_H


/**
 * @struct Pixel
 * 
 * @brief Structure Pixel contenant la couleur du pixel calculée via les données r,g,b.
 * 
*/
struct Pixel{

    unsigned char r,g,b;

    /**
    * @brief Constructeur de la classe : initialise r,g,b à 0.
    * */ 
    Pixel(){r=0;g=0;b=0;};

    /**
    * @brief Constructeur de la classe : initialise r,g,b avec les paramètres nr, ng, nb.
    * @param nr un caractère non signé représentant le rouge.
    * @param ng un caractère non signé représentant le vert.
    * @param nb un caractère non signé représentant le bleu.
    * */ 
    Pixel(unsigned char nr, unsigned char ng, unsigned char nb){
        r=nr;
        g=ng;
        b=nb;
    }
};

#endif