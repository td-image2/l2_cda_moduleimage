#ifndef __IMAGE__H
#define __IMAGE__H



#include "Pixel.h"
#include <string>

using namespace std;

/**
 * @class Image
 * 
 * @brief Classe Image contenant les dimensions dimx et dimy de l'image, deux entiers 
 * et un tableau de pointeurs type Pixel pour la couleur de l'image
 * 
*/
class Image {
    private:
        int dimx;
        int dimy;
        Pixel * tab;
    public:

        /**
        * @brief Constructeur de la classe : initialise dimx et dimy à 0 n’alloue aucune mémoire pour le tableau de pixel
        * */
        Image();

        /**
         * @brief Constructeur de la classe : initialise dimx et dimy (après vérification) puis alloue le tableau de pixel dans le tas (image noire)
         * @param dimx un entier représentant la largeur de l'image
         * @param dimy un entier représentant la hauteur de l'image
         * */
        Image (const int& dimx, const int& dimy);

        /**
         * @brief Destructeur de la classe : déallocation de la mémoire du tableau de pixel et mise à jour des champs dimx et dimy à 0
         * */
        ~Image();

        /**
         * @brief Fonction originale : Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.La formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x]
         * @param x un entier représentant une coordonnée
         * @param y un entier représentant une coordonnée
         * @return des coordonnées
        */
        Pixel& getPix (const int& x, const int& y); //L'original

        /**
         * @brief Copie : Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
         * @param x un entier représentant une coordonnée
         * @param y un entier représentant une coordonnée
         * @return des coordonnées
        */ 
        Pixel getPix (const int& x, const int& y) const; //La copie

        /**
         * @brief Modifie le pixel de coordonnées (x,y)
         * @param x un entier représentant une coordonnée
         * @param y un entier représentant une coordonnée
         * @param couleur la couleur du pixel à remplacer
         * */ 
        void setPix (const int& x, const int& y, const Pixel& couleur);

        /**
         * @brief Dessine un rectangle plein de la couleur dans l'image (en utilisant setPix, indices en paramètre compris)
         * @param Xmin un entier représentant une coordonnée
         * @param Ymin un entier représentant une coordonnée
         * @param Xmax un entier représentant une coordonnée
         * @param Ymax un entier représentant une coordonnée
         * @param couleur donnée de type Pixel qui représente la couleur du rectangle
         * */ 
        void dessinerRectangle(const int& Xmin, const int& Ymin, const int& Xmax, const int& Ymax, const Pixel& couleur);

        /**
         * @brief Efface l'image en la remplissant de la couleur en paramètre (en appelant dessinerRectangle avec le bon rectangle)
         * @param couleur donnée de type Pixel qui représente la couleur utilisée pour remplir
         * */ 
        void effacer (const Pixel& couleur);

        /**
         * @brief Sauve une image dans un fichier
         * @param filename un nom de fichier
         * */
        void sauver(const string &filename) const;

        /**
         * @brief Ouvre une image dans un fichier
         * @param filename un nom de fichier
         * */
        void ouvrir(const string &filename);

        /**
         * @brief Affiche les valeurs des pixels sur la console
         * */
        void afficherConsole();


        /**
         * @brief Récupère la valeur de dimx
         * @return la valeur de dimx
         * */
        int getDimx() const;

        /**
         * @brief Récupère la valeur de dimy
         * @return la valeur de dimy
         * */
        int getDimy() const;

        /**
         * @brief Récupère la valeur de dimy
         * @return le pointeur sur tab
         * */
        void * getTab() const;

        /**
         * @brief Effectue une série de tests vérifiant que toutes les fonctions fonctionnent 
         * et font bien ce qu’elles sont censées faire, 
         * ainsi que les données membres de l'objet sont conformes
         * */
        static void testRegression ();
};

#endif