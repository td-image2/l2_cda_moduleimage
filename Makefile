all: test exemple affichage doc

test: mainTest.o Image.o
	g++ -Wall obj/mainTest.o obj/Image.o -o bin/test

exemple: mainExemple.o Image.o
	g++ -Wall obj/mainExemple.o obj/Image.o -o bin/exemple

affichage: mainAffichage.o ImageViewer.o Image.o
	g++ -Wall obj/mainAffichage.o obj/ImageViewer.o obj/Image.o -o bin/affichage -lSDL2

mainTest.o: src/Image.h src/mainTest.cpp
	g++ -Wall -c src/mainTest.cpp -o obj/mainTest.o

mainExemple.o: src/Image.h src/Pixel.h src/mainExemple.cpp
	g++ -Wall -c src/mainExemple.cpp -o obj/mainExemple.o

mainAffichage.o: src/Image.h src/ImageViewer.h src/mainAffichage.cpp
	g++ -Wall -c src/mainAffichage.cpp -o obj/mainAffichage.o

Image.o: src/Image.cpp src/Image.h
	g++ -Wall -c src/Image.cpp -o obj/Image.o

ImageViewer.o: src/ImageViewer.cpp src/ImageViewer.h src/Image.h
	g++ -Wall -c src/ImageViewer.cpp -o obj/ImageViewer.o

doc : doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm *.o

veryclean: clean
	rm *.out
