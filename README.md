# LIFAPCD TD ModuleImage

## Informations :

**Nom du groupe** : ALTF4

**Noms/Prénoms/Numéros étudiants** : KITA Djessy-Alberto P2209697, KOUAME Côme P2209838, DUCHAMP Lucas P2202838

**Id Projet** : 32786

# Organisation des fichiers/dossiers

-**bin**: Dossier contenant les exécutables crées par le Makefile avec :

   - **test**: Fichier qui exécute mainTest dont il contient testRegression()
   - **exemple**: Exécute mainExemple.cpp qui crée 2 images qui seront lues dans data
   - **affichage**: Exécute mainAffichage.cpp qui oouvre une fenêtre SDL2 contenant le code pour faire une image

-**data**: Les images sauvegardées et lues via mainExemple.cpp

-**doc**: Explications sur les fonctions des fichiers Pixel.h, Image.h et ImageViewer.h

-**obj**: Les fichiers objet des fichiers compilés grâce au Makefile

-**src**: Tous les fichiers sources nécessaires pour le TD ModuleImage

-**Makefile**: Fichier pour compiler les différents programmes avec les commandes : 
 
-    **make**: Compile tous les fichiers, crée les exécutables, fichiers objet et génère la documentation
-    **make doc**: Génère la documentation
-    **make clean**: Efface les fichiers objet
-    **make veryclean**: Efface les exécutables

-**README.md**: Ce readme
